// user.cpp
//

#include "user.hpp"


// STATIC MEMBER INITIALIZATION
const std::string User::EMAIL_FROM_ADDRESS = "no-reply@urlwatcher.net";
const std::string User::EMAIL_REGEX = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}";
const std::string User::EMAIL_SSL_CERT_PATH = "C:\\Server\\cert.pem";

// CONSTRUCTORS
User::User() {}
User::User(std::string ID, std::string email) : _ID(ID), _items()
{
  SetEmail(email); // This is done outside the initializer list in order to trigger validation with regex
}

// SETTERS
TrackingItem* User::AddItem(std::string url, int duration)
{
  // Check for existing item and remove it
  RemoveItemByURL(url);

  // Create new tracking item (which is then automatically validated and has its status set through the constructor)
  _items.emplace(url, TrackingItem(url, duration));

  return GetItemByURL(url);
}
bool User::SetEmail(std::string e)
{
  if (std::regex_match(e, std::regex(EMAIL_REGEX)))
  {
    _email = e;
    return true;
  }
  _email = "";
  return false;
}
bool User::RemoveItemByURL(std::string url)
{
  if (_items.count(url) > 0)
  {
    _items.erase(url);
    return true;
  }
  return false;
}

// GETTERS
std::string User::GetEmail() const
{
  return _email;
}
std::string User::GetID() const
{
  return _ID;
}
TrackingItem* User::GetItemByURL(std::string url)
{
  if (_items.count(url) > 0)
    return &_items.at(url);
  return nullptr;
}
int User::GetNumTracked() const
{
  return _items.size();
}

// TASKS
void User::RemoveAllInactiveItems()
{
  std::map<std::string, TrackingItem>::iterator it = _items.begin();
  while (it != _items.end())
  {
    // Deactivate elapsed items
    it->second.DeactivateIfElapsed();

    // Erase inactive items
    if (!it->second.IsActive())
    {
      it = _items.erase(it);
      continue;
    }

    // Iterate
    ++it;
  }
}
std::string User::RetrieveTrackingReport() const
{
  // Serialize into JSON
  std::ostringstream os;
  {
    cereal::JSONOutputArchive oarchive(os);
    oarchive(_items);
  }

  return os.str();
}
void User::UpdateTrackingItems()
{
  std::string newData = "";
  for (std::map<std::string, TrackingItem>::iterator it = _items.begin(); it != _items.end(); ++it)
  {
    // Send GET request and check if it succeeded
    bool success = it->second.BasicGETRequest(newData);
    if (success)
    {
      // If it did, check if the data received changed
      bool changed = it->second.DataHasChanged(newData);

      // If the data changed and the user has a known email
      if (changed && GetEmail() != "")
      {
        // Get current time and put it into several different local time containers
        time_t t = time(NULL);
        struct tm * now = localtime(&t);
        char *weekday[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
        char day[3], mon[3], hour[3], min[3], sec[3];

        // Prepend 0s if necessary
        now->tm_mday < 10 ? sprintf_s(day, 3, "0%d", now->tm_mday) : sprintf_s(day, 3, "%d", now->tm_mday);
        now->tm_mon  < 10 ? sprintf_s(mon, 3, "0%d", now->tm_mon) : sprintf_s(mon, 3, "%d", now->tm_mon);
        now->tm_hour < 10 ? sprintf_s(hour, 3, "0%d", now->tm_hour) : sprintf_s(hour, 3, "%d", now->tm_hour);
        now->tm_min  < 10 ? sprintf_s(min, 3, "0%d", now->tm_min) : sprintf_s(min, 3, "%d", now->tm_min);
        now->tm_sec  < 10 ? sprintf_s(sec, 3, "0%d", now->tm_sec) : sprintf_s(sec, 3, "%d", now->tm_sec);

        // Create email message in a stringstream
        std::stringstream ss;
        ss << "Date: " << weekday[now->tm_wday] << ", " << day << " " << mon << " " << 1900 + now->tm_year << " " <<
          hour << ":" << min << ":" << sec << " -0500\r\n"
          << "To: <" << GetEmail() << ">" << "\r\n"
          << "From: <" << EMAIL_FROM_ADDRESS << ">(URLWatcher)\r\n"
          << "Message-ID: <" << now->tm_year << now->tm_mon << now->tm_mday << now->tm_hour << now->tm_min << now->tm_sec << rand() % 10000 << "." << EMAIL_FROM_ADDRESS << ">\r\n"
          << "Subject: Change to " << it->second.GetURL() << " detected\r\n"
          << "Content-Type: text/plain; charset=ISO-8859-1\r\n"
          << "\r\n"
          << "URLWatcher has just detected a change at the URL " << it->second.GetURL() << ".\r\n"
          << "This is a complimentary notification.\r\n"
          << "If you would like to stop tracking this URL or any other URL, please go to the Reports tab of our home page in your original browser and click 'Stop Tracking'.\r\n"
          << "\r\n";

        // Send the user a notification of the change
        SendEmail(&ss);
      }
    }
  }
}

// EMAIL FUNCTIONALITY
size_t User::PayloadSource(void *ptr, size_t size, size_t nmemb, void *userp)
{
  std::stringstream* ss = (std::stringstream*)userp;
  char data[256];
  size_t test = size * nmemb;

  if ((size == 0) || (nmemb == 0) || ((size*nmemb) < 1))
  {
    return 0;
  }

  ss->getline(data, 256);

  if (data)
  {
    size_t len = strlen(data);
    memcpy(ptr, data, len);

    return len;
  }

  return 0;
}

bool User::SendEmail(std::stringstream* content)
{
  bool success = false;

  try
  {
    CURL* curl;
    CURLcode res = CURLE_OK;
    struct curl_slist* recipients = NULL;

    curl = curl_easy_init();
    if (curl)
    {
      // Set username and password
      curl_easy_setopt(curl, CURLOPT_USERNAME, "AKIAJ2BPL32KADF35NJA");
      curl_easy_setopt(curl, CURLOPT_PASSWORD, "Albd2RDirS5MCfBvtMyv69vhk+WZiQ2Wf5D4gpq3iJFc");

      // Set url and SSL
      curl_easy_setopt(curl, CURLOPT_URL, "smtp://email-smtp.us-east-1.amazonaws.com:587");
      curl_easy_setopt(curl, CURLOPT_USE_SSL, (long)CURLUSESSL_ALL);

      // Use constants to point to our SSL certificate and our from address
      curl_easy_setopt(curl, CURLOPT_CAINFO, EMAIL_SSL_CERT_PATH);
      curl_easy_setopt(curl, CURLOPT_MAIL_FROM, EMAIL_FROM_ADDRESS.c_str());

      // Add recipients
      recipients = curl_slist_append(recipients, GetEmail().c_str());
      curl_easy_setopt(curl, CURLOPT_MAIL_RCPT, recipients);

      // Use a callback function to specify the payload (the headers and body of the message).
      curl_easy_setopt(curl, CURLOPT_READFUNCTION, PayloadSource);
      curl_easy_setopt(curl, CURLOPT_READDATA, content);
      curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);

      // Send the message
      res = curl_easy_perform(curl);

      // Check for success / errors
      if (res == CURLE_OK)
        success = true;
      else
        std::cerr << "WARNING: Sending email to " << GetEmail() << " failed - error code was: " << curl_easy_strerror(res) << '\n';

      // Free the list of recipients
      curl_slist_free_all(recipients);

      // Always cleanup
      curl_easy_cleanup(curl);
    }
  }
  catch (std::exception ex)
  {
    std::cerr << "WARNING: Sending email to " << GetEmail() << " failed - " << ex.what() << '\n';
  }

  return success;
}
