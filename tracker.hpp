// tracker.hpp
//

#ifndef tracker_hpp
#define tracker_hpp

#include <cstdio>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>

#include <cereal/access.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/string.hpp>

#include "trackingItem.hpp"
#include "user.hpp"


// TODO: Implement some of this class, including the main processing loop
class Tracker
{
private:
  std::map<std::string, User> _users;

  // Serialization
  friend class cereal::access;
  template <class Archive>
  void serialize(Archive& ar)
  {
    ar(_users);
  }
  
public:
  static const int ID_TOKEN_LENGTH;

  // Constructors
  Tracker();
  Tracker(std::map<std::string, User>& users);
  
  // Setters
  User* AddUser(std::string ID, std::string email);
  bool RemoveUserByEmail(std::string email);
  bool RemoveUserByID(std::string ID);
  
  // Getters
  User* GetUserByEmail(std::string email);
  User* GetUserByID(std::string ID);

  // Tasks
  void MainProcess();

  // Static Tasks
  static std::string GenerateID();
};

#endif /* tracker_hpp */
