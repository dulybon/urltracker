// trackingItem.hpp
//

/*
For this class to work properly, both curl and curlpp
have to be setup on your machine.
curl: https://curl.haxx.se/download.html
curlpp: http://www.curlpp.org
*/

#ifndef trackingItem_hpp
#define trackingItem_hpp

#include <ctime>
#include <cstdio>
#include <filesystem>
#include <iomanip>
#include <iostream>
#include <queue>
#include <sstream>
#include <string>
#include <vector>

#include <cereal/access.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/types/queue.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Exception.hpp>
#include <curlpp/Infos.hpp>
#include <curlpp/Options.hpp>


class TrackingItem
{
private:
  std::string _url; // URL to track
  int _duration; // in days (value must be between 1 and 14, inclusive)
  bool _status; // true = active; false = inactive and waiting to be removed
  time_t _initiated;
  time_t _elapsed;
  time_t _lastChange;
  std::string _data;

  // Serialization
  friend class cereal::access;
  template <class Archive>
  void serialize(Archive& ar)
  {
    // Serialize JSON archives (we want to pass: url, status, time remaining, and time of last change)
    if (std::is_same<Archive, cereal::JSONOutputArchive>::value)
      ar(_url, _status, difftime(_elapsed, time(nullptr)), (long long)_lastChange);
    // Serialize all other archives
    else
      ar(_url, _duration, _status, _initiated, _elapsed, _lastChange, _data);
  }
  
public:
  // Constructor
  TrackingItem();
  TrackingItem(std::string url, int duration);
  
  // Setters
  void Deactivate();
  void DeactivateIfElapsed();
  void SetDuration(int dur);
  bool SetStatus();
  
  // Getters
  int GetDuration() const;
  bool GetStatus() const;
  std::string GetURL() const;
  bool IsActive() const;
  
  // Tasks
  bool BasicGETRequest(std::string& requestBodyOutput);
  bool DataHasChanged(std::string& newData);
};

#endif /* trackingItem_hpp */
