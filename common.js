//Implement cookie
cookieValue="";

function getRequestObject() {
  if (window.ActiveXObject)
  {
    return(new ActiveXObject("Microsoft.XMLHTTP"));
  }
  else if (window.XMLHttpRequest)
  {
    return(new XMLHttpRequest());
  }
  else
  {
    return(null);
  }
}


function getCookieFromServer()
{
  
  if ((request.readyState == 4) && (request.status == 200))
  {
    cookieValue = request.responseText;
  }
  else
  {
    console.log("attempt made to get cookie!");
  }

  console.log("cookieValue: " + cookieValue);
  setCookie("trackerID", 60);
}

function sendCookieRequest()
{
  var address = "/newID";
  
  request = getRequestObject();  
  request.onreadystatechange = getCookieFromServer;
  request.open("GET", address, true);
  request.send();
}

function setCookie(cookie_name, exdays) 
{
  var d = new Date();
  
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cookie_name + "=" + cookieValue + "; " + expires;
}

function getCookie(name) {
  var name = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1);
    if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
  }
  return "";
}

function checkCookie() {
  
  var user_cookie = getCookie("trackerID");
  if (user_cookie == "")
  {
    //set the cookie
    sendCookieRequest();
    
    console.log("new cookie created");
    console.log(document.cookie);
    
  }
  else
  {
    if (user_cookie != "")
    {
      console.log("cookie already set");
      console.log(document.cookie);
    }
  }
}

