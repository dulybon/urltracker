// trackingItem.cpp
//

#include "trackingItem.hpp"


// CONSTRUCTORS
// Default constructor
TrackingItem::TrackingItem() {}
TrackingItem::TrackingItem(std::string url, int duration) : _url(url)
{
  // Set up typical variables and validate url
  SetDuration(duration);
  _status = SetStatus();
}

// SETTERS
void TrackingItem::Deactivate()
{
  _status = false;
}
void TrackingItem::DeactivateIfElapsed()
{
  if (IsActive() && time(nullptr) > _elapsed)
    Deactivate();
}
// Duration is in days; it should never be less than 1 or greater than 14
void TrackingItem::SetDuration(int dur)
{
  // Prevent values outside our range
  if (dur < 1)
    dur = 1;
  if (dur > 14)
    dur = 14;

  // Set duration
  _duration = dur;

  // Set up time variables (and destroy our pointer created in the process! NO LEAKS!)
  time(&_initiated);
  time(&_lastChange);
  tm* timeStruct = gmtime(&_initiated);
  timeStruct->tm_mday += _duration;
  _elapsed = mktime(timeStruct);
}
bool TrackingItem::SetStatus()
{
  try
  {
    curlpp::Cleanup cleaner;
    curlpp::Easy request;
    std::ostringstream os;

    // Set up standard options (follow redirects)
    request.setOpt(new curlpp::options::FollowLocation(true));
    request.setOpt(new curlpp::options::MaxRedirs(10));
    request.setOpt(new curlpp::options::Url(GetURL()));
    request.setOpt(new curlpp::options::WriteStream(&os));

    // Perform the request and store the request body as our initial data
    request.perform();
    _data = os.str();

    // Get response code
    long responseCode;
    curlpp::infos::ResponseCode::get(request, responseCode);

    // Handle based on response code - 200 is what we want
    if (responseCode == 200)
    {
      return true;
    }
    else
    {
      std::cout << "INFO: " << GetURL() << " did not resolve properly on our initial request and was not subsequently tracked for that reason. The response code was " << std::to_string(responseCode) << ".\n";
    }
  }
  catch (cURLpp::LogicError & e)
  {
    std::cout << "INFO: " << GetURL() << " did not resolve on our initial request and was not subsequently tracked for that reason. The exception was" << e.what() << '\n';
  }
  catch (cURLpp::RuntimeError & e)
  {
    std::cout << "INFO: " << GetURL() << " did not resolve on our initial request and was not subsequently tracked for that reason. The exception was" << e.what() << '\n';
  }

  return false;
}

// GETTERS
int TrackingItem::GetDuration() const
{
  return _duration;
}
bool TrackingItem::GetStatus() const
{
  return _status;
}
std::string TrackingItem::GetURL() const
{
  return _url;
}
bool TrackingItem::IsActive() const
{
  return GetStatus();
}

// TASKS
/* This function makes an HTTP GET request of the url in question and
and saves the content in a string. This string will serve as
a baseline that subsequent responses can be compared to and check
for change. */
bool TrackingItem::BasicGETRequest(std::string& requestBodyOutput)
{
  bool success = true;

  try
  {
    // Make the request and capture the result
    curlpp::Cleanup cleaner;
    curlpp::Easy request;
    std::ostringstream os;

    // Set up standard options (follow redirects)
    request.setOpt(new curlpp::options::FollowLocation(true));
    request.setOpt(new curlpp::options::MaxRedirs(10));
    request.setOpt(new curlpp::options::Url(GetURL()));
    request.setOpt(new curlpp::options::WriteStream(&os));

    // Perform the request and store the request body as our initial data
    request.perform();
    requestBodyOutput = os.str();
    return true;
  }
  catch (curlpp::LogicError e)
  {
    requestBodyOutput = "";
    std::cerr << "INFO: A LogicError occurred trying to retrieve the URL " << GetURL() << ". Skipping comparison. The exception is " << e.what() << '\n';
    return false;
  }
  catch (curlpp::RuntimeError e)
  {
    requestBodyOutput = "";
    std::cerr << "INFO: A RuntimeError occurred trying to retrieve the URL " << GetURL() << ". Skipping comparison. The exception is " << e.what() << '\n';
    return false;
  }
}
bool TrackingItem::DataHasChanged(std::string& newData)
{
  // Set up stringstreams
  std::stringstream oldResponse, newResponse;
  oldResponse << _data;
  newResponse << newData;

  // Replace old response data with new data now that we have put the old data into an immediately useable format
  _data = newData;

  // Compare each line
  std::string oldLine, newLine;
  while (std::getline(oldResponse, oldLine) && std::getline(newResponse, newLine))
  {
    if (oldLine != newLine)
    {
      time(&_lastChange);
      return true;
    }
  }

  return false;
}
