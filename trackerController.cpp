// trackerController.cpp
//

#include "trackerController.hpp"


// STATIC MEMBER INITIALIZATION
const std::experimental::filesystem::path TrackerController::LATEST_SERIALIZED_MEMORY_DUMP = "C:\\Server\\URLWatcher.data";
const std::experimental::filesystem::path TrackerController::PREVIOUS_SERIALIZED_MEMORY_DUMP = "C:\\Server\\URLWatcher.data.old";

// CONSTRUCTOR
TrackerController::TrackerController()
{
	// Check if backups exist
  bool latestBackupExists = std::experimental::filesystem::exists(LATEST_SERIALIZED_MEMORY_DUMP);
  bool previousBackupExists = std::experimental::filesystem::exists(PREVIOUS_SERIALIZED_MEMORY_DUMP);

  // Attempt to deserialize data
  if (latestBackupExists || previousBackupExists)
  {
    try
    {
      // Create input stream
      std::ifstream backup;
      
      // Rename older file if no latest backup exists
      if (!latestBackupExists)
        std::experimental::filesystem::rename(PREVIOUS_SERIALIZED_MEMORY_DUMP, LATEST_SERIALIZED_MEMORY_DUMP);

      // Open file as binary data
      backup.open(LATEST_SERIALIZED_MEMORY_DUMP, std::ios::binary);

      // Deserialize data
      if (backup.is_open())
      {
        cereal::BinaryInputArchive iarchive(backup);
        iarchive(_tracker);
      }
    }
    catch(std::exception ex)
    {
      // Print deserialization error
      std::cerr << "ERROR: Failed to deserialize data!" << std::endl << ex.what();

      // Set up a default tracker
      _tracker = Tracker();
    }
  }
  else
  {
    // Set up a default tracker
    _tracker = Tracker();
  }
}

// GETTER
Tracker* TrackerController::GetTracker()
{
  return &_tracker;
}

// SERIALIZATION
void TrackerController::SerializeAndSave()
{
  std::cout << "Performing periodic data serialization.\n";

  // Delete old backup if it exists
  if (std::experimental::filesystem::exists(PREVIOUS_SERIALIZED_MEMORY_DUMP))
    std::experimental::filesystem::remove(PREVIOUS_SERIALIZED_MEMORY_DUMP);

  // Rename new backup to old backup
  if (std::experimental::filesystem::exists(LATEST_SERIALIZED_MEMORY_DUMP))
    std::experimental::filesystem::rename(LATEST_SERIALIZED_MEMORY_DUMP, PREVIOUS_SERIALIZED_MEMORY_DUMP);

  // Create new backup
  std::ofstream backup(LATEST_SERIALIZED_MEMORY_DUMP, std::ios::binary);

  // Serialize data
  cereal::BinaryOutputArchive oarchive(backup);
  oarchive(_tracker);
}

// STATIC UTILITY
// Used to get data from .html, .js, .css, etc files
std::string TrackerController::TextFromFile(std::string filePath)
{
  std::string text = "";
  std::ifstream textFile(filePath);
  if (textFile.is_open())
  {
    std::string temp = "";
    while (std::getline(textFile, temp))
      text += temp + '\n';
    textFile.close();
  }

  return text;
}
// Used to split strings over a separator
std::vector<std::string> TrackerController::SplitString(std::string str, std::string separators)
{
  std::vector<std::string> strs;
  boost::split(strs, str, boost::is_any_of(separators));

  // Iterate over the vector, replacing all the JS 'null' and 'undefined' sections with an empty string for easier comparisons
  for (std::string str : strs)
  {
    if (str == "undefined" || str == "null")
      str = "";
  }

  return strs;
}
