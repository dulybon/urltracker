// server.cpp
// 

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <thread>

#include <boost/lexical_cast.hpp>
#include <boost/network/include/http/server.hpp>
#include <boost/thread.hpp>

#include "tracker.hpp"
#include "trackerController.hpp"
#include "trackingItem.hpp"
#include "user.hpp"

namespace http = boost::network::http;


// Defines the server
struct Routes;
typedef http::server<Routes> server;


struct Routes
{
private:
  const std::string ROOT_FOLDER = "C:\\Server\\Root\\";
  const std::map<std::string, std::pair<std::string, std::string>> paths = {
    { "", { ROOT_FOLDER + "trackUrl.html", "text/html" } },
    { "/", { ROOT_FOLDER + "trackUrl.html", "text/html" } },
    { "/bootstrap.min.css", { ROOT_FOLDER + "bootstrap.min.css", "text/css" } },
    { "/bootstrap.min.css.map", { ROOT_FOLDER + "bootstrap.min.css.map", "application/octet-stream" } },
    { "/common.js", { ROOT_FOLDER + "common.js", "application/javascript" } },
    { "/report.html", { ROOT_FOLDER + "report.html", "text/html" } },
    { "/report.js", { ROOT_FOLDER + "report.js", "application/javascript" } },
    { "/trackUrl.html", { ROOT_FOLDER + "trackUrl.html", "text/html" } },
    { "/trackUrl.js", { ROOT_FOLDER + "trackUrl.js", "application/javascript" } },
    { "/validateInputs.js", { ROOT_FOLDER + "validateInputs.js", "application/javascript" } },
  };

  Tracker* _tracker;

public:
  Routes(Tracker* tracker) : _tracker(tracker)
  {}

  // This function handles all incoming requests
  void operator()(server::request const &request, server::response &response)
  {
    // Break out values for IP address, port, url, and request body
    server::string_type ip = source(request);
    unsigned int port = request.source_port;
    std::string url = request.destination;
    std::string body = request.body;

    // Response variables
    server::response::status_type status;
    std::string data;

    // RESPOND TO A KNOWN PATH WITH A LOCAL FILE
    if (paths.count(url) > 0)
    {
      // Read from sepcified file and set mime type if this is a known standard path
      data = TrackerController::TextFromFile(paths.at(url).first);

      // Set status as 'ok'
      status = server::response::ok;
    }
    // RESPOND TO A PROPER 'GET NEW COOKIE ID' REQUEST
    else if (url == "/newID")
    {
      // Generate new ID and set it to the user as plain text
      data = _tracker->GenerateID();

      // Set status as 'ok'
      status = server::response::ok;
    }
    // RESPOND TO A PROPER 'START TRACKING URL' REQUEST
    else if (url == "/startTracking")
    {
      // Split request body into strings
      std::vector<std::string> splitBody = TrackerController::SplitString(body, ",");

      // Make sure the ID string is real (this avoids trying to read outside the index of the string if there is small, invalid input)
      if (splitBody.size() == 4 && splitBody[0].length() == (size_t)(Tracker::ID_TOKEN_LENGTH + 10)) // Length needs to equal the token length plus the length of "trackerID="
      {
        // Define some local variables out of the positions in the vector we are expecting to be occupied
        std::string ID = splitBody[0].substr(10);
        std::string url = splitBody[1];
        std::string email = splitBody[2];
        int duration = std::stoi(splitBody[3]);

        // From ID, get user or create new user
        User* currentUser = _tracker->GetUserByID(ID);
        if (currentUser == nullptr)
          currentUser = _tracker->AddUser(ID, email);

        // Create new tracking item
        currentUser->AddItem(url, duration);
        data = std::to_string(1);
      }
      else
      {
        std::cerr << "WARNING: Invalid request to /startTracking. Body was " << body << " resulting in a string vector of size " << splitBody.size() << ".\n";
        data = std::to_string(0);
      }

      // Set status as 'ok'
      status = server::response::ok;
    }
    // RESPOND TO A PROPER 'STOP TRACKING URL' REQUEST
    else if (url == "/stopTracking")
    {
      // Split request body into strings
      std::vector<std::string> splitBody = TrackerController::SplitString(body, ",");

      // Make sure the ID string is real (this avoids trying to read outside the index of the string if there is small, invalid input)
      if (splitBody.size() == 2 && splitBody[0].length() == (size_t)(Tracker::ID_TOKEN_LENGTH + 10)) // Length needs to equal the token length plus the length of "trackerID="
      {
        // Define some local variables out of the positions in the vector we are expecting to be occupied
        std::string ID = splitBody[0].substr(10);
        std::string url = splitBody[1];

        // From ID, get user or abort
        User* currentUser = _tracker->GetUserByID(ID);
        if (currentUser != nullptr)
          data = std::to_string((int)currentUser->RemoveItemByURL(url));
        else
          data = std::to_string(0);
      }
      else
      {
        std::cerr << "WARNING: Invalid request to /stopTracking. Body was " << body << " resulting in a string vector of size " << splitBody.size() << ".\n";
        data = std::to_string(0);
      }

      // Set status as 'ok'
      data = server::response::ok;
    }
    // RESPOND TO A PROPER 'Update TRACKING REPORT' REQUEST
    else if (url == "/updateReport")
    {
      // Make sure the ID string is real (this avoids trying to read outside the index of the string if there is small, invalid input)
      if (body.length() == (size_t)(Tracker::ID_TOKEN_LENGTH + 10)) // Length needs to equal the token length plus the length of "trackerID="
      {
        std::string ID = body.substr(10);

        User* currentUser = _tracker->GetUserByID(ID);
        if (currentUser != nullptr)
          data = currentUser->RetrieveTrackingReport();
        else
          data = std::to_string(0);
      }
      else
      {
        std::cerr << "WARNING: Invalid request to /updateReport. Body was " << body << ".\n";
        data = std::to_string(0);
      }

      // Set status as 'ok'
      status = server::response::ok;
    }
    // RESPOND TO ANYTHING ELSE WITH A 404 AND AN ERROR MESSAGE AS CONTENT
    else
    {
      data = "The page '" + url + "' could not be found on the server!";
      status = server::response::not_found;
    }

    // Set status and write data
    response = server::response::stock_reply(status, data);
  }

  void log(server::string_type const &info)
  {
    std::cerr << "ERROR: " << info << '\n';
  }
};


int main(int argc, char *argv[])
{
  // Constants for the main program threads
  const int DEFAULT_MAIN_PROCESSING_THREAD_INTERVAL = 1; // in minutes
  const int DEFAULT_SERIALIZATION_THREAD_INTERVAL = 5; // in minutes

  try
  {
    // Periodically serialize data
    TrackerController controller = TrackerController();
    std::thread serializationThread([](TrackerController* controller, int duration) {
      while (true)
      {
        try
        {
          std::this_thread::sleep_for(std::chrono::minutes(duration));
          controller->SerializeAndSave();
        }
        catch (std::exception ex)
        {
          std::cerr << "ERROR: Caught unhandled exception in periodic serialization code. Exception was " << ex.what() << '\n';
        }
      }
    }, &controller, DEFAULT_SERIALIZATION_THREAD_INTERVAL);

    // Spawn thread for main process
    Tracker* tracker = controller.GetTracker();
    std::thread mainProcessThread([](Tracker* tracker, int duration) {
      while (true)
      {
        try
        {
          std::this_thread::sleep_for(std::chrono::minutes(duration));
          tracker->MainProcess();
        }
        catch (std::exception ex)
        {
          std::cerr << "ERROR: Caught unhandled exception in periodic processing code. Exception was " << ex.what() << '\n';
        }
      }
    }, tracker, DEFAULT_MAIN_PROCESSING_THREAD_INTERVAL);

    // Creates the request handler
    Routes handler(tracker);

    // Create the server
    server::options options(handler);
    server server_(options.address("0.0.0.0").port("80"));

    try
    {
      // Run the server
      server_.run();
    }
    catch (std::exception ex)
    {
      std::cerr << "ERROR: Caught unhandled exception in main server code. Exception was " << ex.what() << '\n';
    }
  }
  catch (std::exception &ex)
  {
    std::cerr << ex.what() << std::endl;
    return 1;
  }

  return 0;
}
