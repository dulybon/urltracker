// user.hpp
//

#ifndef user_hpp
#define user_hpp

#include <cstdio>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>
#include <vector>

#include <cereal/access.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/string.hpp>

#include "trackingItem.hpp"


class User
{
private:
  static const std::string EMAIL_FROM_ADDRESS;
  static const std::string EMAIL_REGEX;
  static const std::string EMAIL_SSL_CERT_PATH;

  std::string _ID;
  std::string _email;
  std::map<std::string, TrackingItem> _items;

  // Serialization
  friend class cereal::access;
  template <class Archive>
  void serialize(Archive& ar)
  {
    ar(_ID, _email, _items);
  }
  
public:
  // Constructors
  User();
  User(std::string ID, std::string email);
  
  // Setters
  TrackingItem* AddItem(std::string url, int duration);
  bool SetEmail(std::string e);
  bool RemoveItemByURL(std::string url);
  
  // Getters
  std::string GetEmail() const;
  std::string GetID() const;
  TrackingItem* GetItemByURL(std::string url);
  int GetNumTracked() const;
  
  // Tasks
  void RemoveAllInactiveItems();
  std::string RetrieveTrackingReport() const;
  void UpdateTrackingItems();

  // Email Functionality
  static size_t PayloadSource(void* ptr, size_t size, size_t nmemb, void* userp);
  bool SendEmail(std::stringstream* content);
};

#endif /* user_hpp */
