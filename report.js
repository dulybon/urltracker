//Expected string from server [url, duration, frequency, status]
var data = "www.cnn.com, 5, 5,ACTIVE";
var currentPanel = 1;
var stringFromServer= "";



function getUrlFromJsonString(jsonString, index)
{
  var obj = JSON.parse(jsonString);
  var url = obj.value0[index].key;
  return url;
}

function getStatusFromJsonString(jsonString, index)
{
  var obj = JSON.parse(jsonString);
  var status = obj.value0[index].value.value1;
  return status;
}

function getTimeLeftFromJsonString(jsonString, index)
{
  var obj = JSON.parse(jsonString);
  var timeLeft = obj.value0[index].value.value2;
  return timeLeft;
}

function getLastTimeCheckedFromJsonString(jsonString, index)
{
  var obj = JSON.parse(jsonString);
  var lastChecked = obj.value0[index].value.value3;
  return lastChecked;
}

function toDateTime(secs) {
  var t = new Date(1970, 0, 1); // Epoch
  t.setSeconds(secs);
  return t;
}

function printLastChecked(seconds)
{
  var date = toDateTime(seconds);
  var dtString = date.toLocaleDateString() +" " + date.toLocaleTimeString();
  return dtString;
}

function printTimeLeftInDays(seconds)
{
  if(seconds < 0 )
  {
    seconds *= -1;
  }
  var days = seconds/(60*60*24);
  days = Math.floor(days);
  
  if(days <= 0)
  {
    return "Less Than 1 day left"
  }
  else if(days == 1)
  {
    return "1 day left";
  }
  else
  {
    return days + " days left"
  }
  
}

function makeObjectToFillPanel(jsonData,index)
{
  var trackingItem = {};
  var url = getUrlFromJsonString(jsonData,index);
  var stat = getStatusFromJsonString(jsonData, index);
  var timeLeft = getTimeLeftFromJsonString(jsonData,index);
  var lastChecked = getLastTimeCheckedFromJsonString(jsonData, index);
  
  trackingItem._url = url;
  trackingItem._status = stat;
  trackingItem._timeLeft = printTimeLeftInDays (timeLeft);
  trackingItem._lastChecked = printLastChecked(lastChecked);
  
  return trackingItem;
  
}

//get data from stored session
function getStoredSession()
{
  return sessionStorage.getItem("email");
}


//get request object
function getRequestObject() {
  if (window.ActiveXObject)
  {
    return(new ActiveXObject("Microsoft.XMLHTTP"));
  }
  else if (window.XMLHttpRequest)
  {
    return(new XMLHttpRequest());
  }
  else
  {
    return(null);
  }
}

function showResponseAlert()
{
  if ((request.readyState == 4) && (request.status == 200))
  {
    stringFromServer = request.responseText;
    //alert(request.responseText);
  }
  else
  {
    console.log("Attempt was made!!");
  }
}

function splitStringFromServer(serverString)
{
  var splittedServerString = serverString.split("\n");
  return splittedServerString;
}

/*
 This request sends the email to server
 It expect a list of strings in the form
 "www.cnn.com, 5, 5, ACTIVE" in return, at
 most 10 lines
 */
function sendRequest()
{
  //place to send
  address = "http://urlwatcher.net/email.txt";
  
  //get data from webpage
  var data = getStoredSession();
  
  //console.log(data);
  //send data to server

  request = getRequestObject();
  request.onreadystatechange = showResponseAlert;
  request.open("POST", address, true);
  request.send(data);
}

function updateThePage()
{
  if ((updateRequest.readyState == 4) && (updateRequest.status == 200))
  {
    hidePanels();
    hideButtons();

    //this is an array
    var dataFromJson= updateRequest.responseText;

    //console.log(updateRequest.responseText); 
    console.log(dataFromJson["value0"]);
    
    for(i = 0; i <JSON.parse(dataFromJson).value0.length; i++)
    {
       var obj = makeObjectToFillPanel(dataFromJson, i)
	  console.log(obj);

       fillPanel(obj);
    }
    
  }
  else
  {
    console.log("attampt made to update the page each minute!");
  }
  currentPanel = 1;
}
function sendUpdateRequest()
{
  var address = "/updateReport";
  updateRequest = getRequestObject();
  
  var data = document.cookie;
  
  updateRequest.onreadystatechange = updateThePage;
  updateRequest.open("POST", address, true);
  updateRequest.send(data);
}
function getButtons()
{
  var buttons = document.getElementsByClassName("stopButton");
  return buttons;
}

function getPanels()
{
  var panels = document.getElementsByClassName("panel");
  return panels;
}


function hidePanels()
{
  var panels = getPanels();
  var l = panels.length;
  for(i = 0; i < l; i++)
  {
    panels[i].style.visibility = "hidden";
  }
}

function hideButtons()
{
  var buttons = getButtons();
  for(i = 0; i < buttons.length; i++)
  {
    buttons[i].style.visibility = "hidden";
  }
}

function showButtonWithId(buttonId)
{
  var button = document.getElementById(buttonId);
  button.style.visibility = "visible";
}

function showPanelWithId(id)
{
 var panel = document.getElementById(id);
 panel.style.visibility = "visible";
}

function makeObjFromString(serverString)
{
  var splitted = serverString.split(",");
  var obj = {url: splitted[0], duration:splitted[1], frequency:splitted[2],status: splitted[3]};
  
  return obj;
}

function printObjectTitle(obj)
{
  var urlLine = "<b>" + obj._url + "<b>";
  return urlLine;
}
function printObjectBody(obj)
{
  
  var durationLine = "Days Of Tracking Left: " + obj._timeLeft + "<br>";
  var lastCheckedTime = "Last Update: " + obj._lastChecked + "<br>";
  var statusLine = "Current Status: " + obj._status + "<br>";
  
  return durationLine + lastCheckedTime + statusLine;
}

function fillPanel(obj)
{
  var panel = document.getElementById("panel-" + currentPanel);
  if(obj._status == true)
  {
    panel.className += " panel-primary";
    showButtonWithId("stopButton-" + currentPanel);

  }
  else
  {
    panel.className += " panel-warning";
  }
  
  var panelTitle = document.getElementById("panel-title-" + currentPanel);
  var panelbody = document.getElementById("panel-body-" + currentPanel);
  panelTitle.innerHTML = printObjectTitle(obj);
  panelbody.innerHTML = printObjectBody(obj);
  
  showPanelWithId("panel-" + currentPanel);
  currentPanel++;
  
}

function disapear()
{
  var messageDisplay=document.getElementById("stopTrackingUrl");
  messageDisplay.style.visibility = "hidden";
  
}
function trackingWillStop(url)
{
  var messageDisplay = document.getElementById("stopTrackingUrl");
  messageDisplay.style.visibility = "visible";
  messageDisplay.style.color = "green";

  if ((stopTrackingRequest .readyState == 4) && (stopTrackingRequest .status == 200))
  {
    messageDisplay.innerHTML = "tracking will stop for " + url;
    console.log("tracking will stop for " + url);
  }
  else
  {
    messageDisplay.innerHTML = "unsuccesful attempt to stop tracking "+ url;
    console.log("unsuccesful attempt to stop tracking "+ url);
  }

  setTimeout(disapear, 10000);
}
function stopTracking(paneltitleId)
{
  var panelTitle = document.getElementById(paneltitleId);
  var url = panelTitle.innerText;

  var address = "/stopTracking";

  var data = document.cookie + "," + url;

  stopTrackingRequest = getRequestObject();
  stopTrackingRequest.onreadystatechange = trackingWillStop(url);
  stopTrackingRequest.open("POST", address, true);
  stopTrackingRequest.send(data);
  console.log("url: " + url);
  console.log("data: " + data);
}

