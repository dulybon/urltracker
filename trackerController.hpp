// trackerController.hpp
//

#ifndef trackerController_hpp
#define trackerController_hpp

#include <cstdio>
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <cereal/access.hpp>
#include <cereal/archives/binary.hpp>

#include "tracker.hpp"


class TrackerController
{
private:
	static const std::experimental::filesystem::path LATEST_SERIALIZED_MEMORY_DUMP;
	static const std::experimental::filesystem::path PREVIOUS_SERIALIZED_MEMORY_DUMP;

  Tracker _tracker;

public:
  // Constructor
  TrackerController();

  // Getter
  Tracker* GetTracker();

  // Serialization
  void SerializeAndSave();

  // Static Utility
  static std::string TextFromFile(std::string filePath);
  static std::vector<std::string> SplitString(std::string str, std::string separators);
};

#endif /* trackerController_hpp */
