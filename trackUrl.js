//redirect user to report page
function redirectToReportPage()
{
  var location = "/report.html"
  window.location = location;
}


function getRequestObject() {
  if (window.ActiveXObject)
  {
    return(new ActiveXObject("Microsoft.XMLHTTP"));
  }
  else if (window.XMLHttpRequest)
  {
    return(new XMLHttpRequest());
  }
  else
  {
    return(null);
  }
}

//show message received from response
function showResponseAlert()
{
  if ((request.readyState == 4) && (request.status == 200))
  {
    
    console.log(request.responseText);
    if(request.responseText == "1")
    {
      //success
      redirectToReportPage();
      
    }
  }
  else
  {
    var errorLine = document.getElementById("inputError-1")
    errorLine.innerHTML ="Tracking Not Successful!";
    errorLine.style.color = "red";

    console.log("Attempt was made!!");
  }
}

function getUserName(email)
{
  var mail = email;
  var splitted = mail.split("@");
  return splitted;
}

function sendRequest()
{
  if(!validateEmail())
  {}
  else
  {
    //place to send
    address = "/startTracking";
    
    //get data from webpage
    var urlToTrack = document.getElementById("InputUrl1").value;
    var email = document.getElementById("InputEmail1").value;
    var duration = document.getElementById("sel1").value;
    
    var data = document.cookie + "," + urlToTrack + "," + email + "," + duration;
    
    console.log(data);
    
    //send data to server
    request = getRequestObject();
    request.onreadystatechange = showResponseAlert;
    request.open("POST", address, true);
    request.send(data);
  }
}

function storeSessionInfo()
{
  sessionStorage.setItem("email", document.getElementById("InputEmail1").value);
  sessionStorage.setItem("url", document.getElementById("InputUrl1").value);
}



