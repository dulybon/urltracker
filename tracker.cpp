// tracker.cpp
// 

#include "tracker.hpp"


// STATIC MEMBER INITIALIZATION
const int Tracker::ID_TOKEN_LENGTH = 24;

// CONSTRUCTOR
Tracker::Tracker() {}
Tracker::Tracker(std::map<std::string, User>& users) : _users(users) {}

// SETTERS
User* Tracker::AddUser(std::string ID, std::string email)
{
  _users.emplace(ID, User(ID, email));
  return GetUserByID(ID);
}
bool Tracker::RemoveUserByEmail(std::string email)
{
  for (std::map<std::string, User>::iterator it = _users.begin(), end = _users.end(); it != end; ++it)
  {
    if (it->second.GetEmail() == email)
    {
      it = _users.erase(it);
      return true;
    }
  }
  return false;
}
bool Tracker::RemoveUserByID(std::string ID)
{
  if (_users.count(ID) > 0)
  {
    _users.erase(ID);
    return true;
  }
  return false;
}

// GETTERS
User* Tracker::GetUserByEmail(std::string email)
{
  for (std::map<std::string, User>::iterator it = _users.begin(), end = _users.end(); it != end; ++it)
  {
    if (it->second.GetEmail() == email)
      return &it->second;
  }
  return nullptr;
}
User* Tracker::GetUserByID(std::string ID)
{
  if (_users.count(ID) > 0)
    return &_users.at(ID);
  else
    return nullptr;
}

// TASKS
void Tracker::MainProcess()
{
  std::map<std::string, User>::iterator it = _users.begin();
  while (it != _users.end())
  {
    // Deactivate elapsed items and delete inactive items for all users
    it->second.RemoveAllInactiveItems();

    // Delete inactive users
    if (it->second.GetNumTracked() == 0)
    {
      it = _users.erase(it);
      continue;
    }

    // Update data and make comparisons
    it->second.UpdateTrackingItems();

    // Iterate
    ++it;
  }
}

// STATIC TASKS
std::string Tracker::GenerateID()
{
  auto randchar = []() -> char
  {
    const char charset[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    const size_t max_index = (sizeof(charset) - 1);
    return charset[rand() % max_index];
  };
  std::string str(ID_TOKEN_LENGTH, 0);
  std::generate_n(str.begin(), ID_TOKEN_LENGTH, randchar);
  return str;
}
