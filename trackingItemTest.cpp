//
//  trackingItemTest.cpp
//  URLTRACKER_CLASSES
//
//  CEN4021
//  DKB14
//  Duly Bonheur
//
//  This program tests the functions of
//  TrackingItem class.
//
//  Note: TrackingItem::getURL() function cannot be
//  properly tested at this point. Expected functionality
//  of this function requires that the code be able to make
//  HTTP requests to verify the validity of Url(s). That
//  had not been added to the application. When that is
//  done, only then could this function be testec for
//  correct functionality.
//
//
//  Created by Duly Bonheur on 3/23/16.
//

#include <cassert>
#include <iostream>

#include "tracker.hpp"
#include "trackingItem.hpp"
#include "user.hpp"


int main(int argc, const char * argv[]) {
  
  //test the constructors
  TrackingItem item1("www.google.com");
  assert(item1.getURL() == "www.google.com");
  assert(item1.getDuration() == 1);
  assert(item1.getFrequency() == 5);
  assert(item1.getStatus() == "INACTIVE");
  
  
  TrackingItem item2("www.yahoo.com", 2);
  assert(item2.getURL() == "www.yahoo.com");
  assert(item2.getDuration() == 2);
  assert(item2.getFrequency() == 5);
  assert(item2.getStatus() == "INACTIVE");
  
  //test copy constructor
  TrackingItem item3(item2);
  assert(item2.getURL()==item3.getURL());
  assert(item2.getDuration() == item3.getDuration());
  assert(item2.getStatus()==item3.getStatus());
  assert(item2.getFrequency() == item3.getFrequency());
  
  //test assignment operator
  TrackingItem item4 = item1;
  assert(item1.getURL()==item4.getURL());
  assert(item1.getFrequency()==item4.getFrequency());
  assert(item1.getDuration()==item4.getDuration());
  assert(item1.getStatus()==item4.getStatus());
  
  
  //testing TrackingItem::_url getter and setter
  //TO DO: when HTTP requests are possible
  
  //testing TrackingItem::_duration getter and setter
  bool durationSetItem10 = item1.setDuration(0);
  assert(durationSetItem10 == false);
  assert(item1.getDuration() == 1);
  
  bool durationSetItem28 = item2.setDuration(8);
  assert(durationSetItem28 == false);
  assert(item2.getDuration() == 2);
  
  bool durationSetItem34 = item3.setDuration(4);
  assert(durationSetItem34 == true);
  assert(item3.getDuration() == 4);
  
  bool durationSetItem41 = item4.setDuration(1);
  assert(durationSetItem41 == true);
  assert(item4.getDuration() == 1);
  
  bool durationSetItem17 = item1.setDuration(7);
  assert(durationSetItem17 == true);
  assert(item1.getDuration() == 7);
  
  //testing TrackingItem::_frequency getter and setter
  bool frequencyItem14 = item1.setFrequency(4);
  assert(frequencyItem14 == false);
  assert(item1.getFrequency() == 5);
  
  bool frequencyItem261 = item2.setFrequency(61);
  assert(frequencyItem261 == false);
  assert(item2.getFrequency() == 5);
  
  bool frequencyItem35 = item3.setFrequency(5);
  assert(frequencyItem35 == true);
  assert(item3.getFrequency() == 5);
  
  bool frequencyItem460 = item4.setFrequency(60);
  assert(frequencyItem460 == true);
  assert(item4.getFrequency() == 60);
  
  bool frequencyItem110 = item1.setFrequency(10);
  assert(frequencyItem110 == true);
  assert(item1.getFrequency() == 10);
  
  bool frequencyItem220 = item2.setFrequency(20);
  assert(frequencyItem220 == true);
  assert(item2.getFrequency() == 20);
  
  //testing TrackingItem::_status getter and setter
  bool statusItem1_active = item1.setStatus("active");
  assert(statusItem1_active == false);
  assert(item1.getStatus() == "INACTIVE");
  
  bool statusItem2_empty = item2.setStatus("");
  assert(statusItem2_empty == false);
  assert(item2.getStatus() == "INACTIVE");
  
  bool statusItem3_inactive = item3.setStatus("inactive");
  assert(statusItem3_inactive == false);
  assert(item3.getStatus() == "INACTIVE");
  
  bool statusItem4_other = item4.setStatus("other");
  assert(statusItem4_other == false);
  assert(item4.getStatus() == "INACTIVE");
  
  bool statusItem1_ACTIVE = item1.setStatus("ACTIVE");
  assert(statusItem1_ACTIVE == true);
  assert(item1.getStatus() == "ACTIVE");
  
  bool statusItem2_INACTIVE = item2.setStatus("INACTIVE");
  assert(statusItem2_INACTIVE == true);
  assert(item2.getStatus() == "INACTIVE");
  
  bool statusItem3_INACTIVE = item3.setStatus("INACTIVE");
  assert(statusItem3_INACTIVE == true);
  assert(item3.getStatus() == "INACTIVE");
  
  bool statusItem4_ACTIVE = item4.setStatus("ACTIVE");
  assert(statusItem4_ACTIVE == true);
  assert(item4.getStatus() == "ACTIVE");
  
  // testing TrackingItem::activateTracking() and
  // TrackingItem::deactivateTracking()
  
  //activate all
  item1.activateTracking();
  item2.activateTracking();
  item3.activateTracking();
  item4.activateTracking();
  
  assert(item1.getStatus() == item2.getStatus() &&
         item3.getStatus() == item4.getStatus());
  
  assert(item2.getStatus() == "ACTIVE");
  
  //deactivate all
  item1.deactivateTracking();
  item2.deactivateTracking();
  item3.deactivateTracking();
  item4.deactivateTracking();
  
  assert(item1.getStatus() == item2.getStatus() &&
         item3.getStatus() == item4.getStatus());
  
  assert(item2.getStatus() == "INACTIVE");
  
  item1.printItem();
  item2.printItem();
  item3.printItem();
  item4.printItem();
  
  return 0;
}

